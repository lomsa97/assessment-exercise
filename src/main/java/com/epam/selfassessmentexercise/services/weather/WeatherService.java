package com.epam.selfassessmentexercise.services.weather;

import com.epam.selfassessmentexercise.models.entities.Countries;
import com.epam.selfassessmentexercise.models.entities.Weathers;
import com.epam.selfassessmentexercise.models.requests.CurrentWeatherRequest;
import com.epam.selfassessmentexercise.models.responses.weather.WeatherResponse;
import com.epam.selfassessmentexercise.models.responses.weather.api.CurrentWeatherModelResponse;
import com.epam.selfassessmentexercise.models.responses.weather.api.CurrentWeatherResponse;

import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

public interface WeatherService {

    List<WeatherResponse> getSavedHistoryWeatherData();

    WeatherResponse fetchDataFromCurrentWeather(CurrentWeatherRequest currentWeatherRequest);

    Supplier<Countries> generateNewCountry(CurrentWeatherRequest currentWeatherRequest, Weathers weather);

    Function<Countries, List<WeatherResponse>> convertCountriesToWeatherResponse();

    Function<Weathers, WeatherResponse> convertWeathersToWeatherResponse(String countryName, String cityName);

    Function<CurrentWeatherModelResponse, Weathers> convertCurrentWeatherModelResponseToWeathers(LocalDateTime localDateTime);

    CurrentWeatherResponse fetchDataFromCurrentWeatherApi(CurrentWeatherRequest currentWeatherRequest);
}
