package com.epam.selfassessmentexercise.services.weather;

import com.epam.selfassessmentexercise.models.entities.Cities;
import com.epam.selfassessmentexercise.models.entities.Countries;
import com.epam.selfassessmentexercise.models.entities.Weathers;
import com.epam.selfassessmentexercise.models.properties.WeatherApiProperties;
import com.epam.selfassessmentexercise.models.requests.CurrentWeatherRequest;
import com.epam.selfassessmentexercise.models.responses.weather.WeatherResponse;
import com.epam.selfassessmentexercise.models.responses.weather.api.CurrentWeatherModelResponse;
import com.epam.selfassessmentexercise.models.responses.weather.api.CurrentWeatherResponse;
import com.epam.selfassessmentexercise.repositories.CountiesRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class WeatherServiceImpl implements WeatherService {

    private final RestTemplate restTemplate;
    private final CountiesRepo countiesRepo;
    private final WeatherApiProperties weatherApiProperties;

    @Autowired
    public WeatherServiceImpl(RestTemplate restTemplate,
                              CountiesRepo countiesRepo,
                              WeatherApiProperties weatherApiProperties) {
        this.restTemplate = restTemplate;
        this.countiesRepo = countiesRepo;
        this.weatherApiProperties = weatherApiProperties;
    }

    @Override
    @Transactional(readOnly = true)
    public List<WeatherResponse> getSavedHistoryWeatherData() {
        return countiesRepo.findAll()
                .stream()
                .map(convertCountriesToWeatherResponse())
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public WeatherResponse fetchDataFromCurrentWeather(CurrentWeatherRequest currentWeatherRequest) {
        Weathers weather = fetchDataFromCurrentWeatherApi(currentWeatherRequest)
                .getData()
                .stream()
                .map(convertCurrentWeatherModelResponseToWeathers(LocalDateTime.now())).findFirst().orElseThrow();

        Countries country = countiesRepo.findByName(currentWeatherRequest.getCountry());

        if (country == null) {
            country = generateNewCountry(currentWeatherRequest, weather).get();
        } else {
            Cities city = country.getCities().stream()
                    .filter(currentCity -> currentCity.getName().equals(currentWeatherRequest.getCity()))
                    .findFirst()
                    .orElse(null);
            if (city == null) {
                Cities newCity = new Cities();
                newCity.setName(currentWeatherRequest.getCity());
                newCity.getWeathers().add(weather);
                country.getCities().add(newCity);
            } else {
                city.getWeathers().add(weather);
            }
        }

        countiesRepo.save(country);

        return convertWeathersToWeatherResponse(currentWeatherRequest.getCountry(), currentWeatherRequest.getCity())
                .apply(weather);
    }

    @Override
    public Supplier<Countries> generateNewCountry(CurrentWeatherRequest currentWeatherRequest, Weathers weather) {
        return () -> {
            Countries newCountry = new Countries();
            Cities newCity = new Cities();

            newCountry.setName(currentWeatherRequest.getCountry());
            newCity.setName(currentWeatherRequest.getCity());

            newCity.setWeathers(Set.of(weather));
            newCountry.setCities(Set.of(newCity));

            return newCountry;
        };
    }

    @Override
    public Function<Countries, List<WeatherResponse>> convertCountriesToWeatherResponse() {
        return countries -> {
            List<WeatherResponse> weatherResponseList = new ArrayList<>();

            countries.getCities()
                    .forEach(city -> city.getWeathers()
                            .forEach(weather -> weatherResponseList.add(convertWeathersToWeatherResponse(countries.getName(), city.getName()).apply(weather))));

            return weatherResponseList;
        };
    }

    @Override
    public Function<Weathers, WeatherResponse> convertWeathersToWeatherResponse(String countryName, String cityName) {
        return weather -> {
            WeatherResponse weatherResponse = new WeatherResponse();

            weatherResponse.setCountryName(countryName);
            weatherResponse.setCityName(cityName);
            weatherResponse.setWindDir(weather.getWindDir());
            weatherResponse.setSunset(weather.getSunset());
            weatherResponse.setSunrise(weather.getSunrise());
            weatherResponse.setTemp(weather.getTemp());
            weatherResponse.setDateTime(weather.getDateTime());
            weatherResponse.setSysdate(weather.getSysdate());

            return weatherResponse;
        };
    }

    @Override
    public Function<CurrentWeatherModelResponse, Weathers> convertCurrentWeatherModelResponseToWeathers(LocalDateTime localDateTime) {
        return currentWeatherModelResponse -> {
            Weathers weather = new Weathers();

            weather.setSunrise(currentWeatherModelResponse.getSunrise());
            weather.setWindSpeed(currentWeatherModelResponse.getWindSpeed());
            weather.setWindDir(currentWeatherModelResponse.getWindDir());
            weather.setSunset(currentWeatherModelResponse.getSunset());
            weather.setSunrise(currentWeatherModelResponse.getSunrise());
            weather.setTemp(currentWeatherModelResponse.getTemp());
            weather.setDateTime(currentWeatherModelResponse.getDateTime());
            weather.setSysdate(localDateTime);

            return weather;
        };
    }

    @Override
    public CurrentWeatherResponse fetchDataFromCurrentWeatherApi(CurrentWeatherRequest currentWeatherRequest) {
        String url = String.format(weatherApiProperties.getEndPointCurrentWeather(), currentWeatherRequest.getCity(), currentWeatherRequest.getCountry());
        ResponseEntity<CurrentWeatherResponse> result = restTemplate.exchange(url, HttpMethod.GET, null, CurrentWeatherResponse.class);
        return result.getBody();
    }
}
