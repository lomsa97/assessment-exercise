package com.epam.selfassessmentexercise.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter

@Entity
@Table(name = "COUNTIES",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
public class Countries {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "city_id", referencedColumnName = "id", nullable = false)
    private Set<Cities> cities = new HashSet<>();

    @Column(name = "name", nullable = false)
    private String name;
}
