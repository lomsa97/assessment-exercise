package com.epam.selfassessmentexercise.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter

@Entity
@Table(name = "WEATHERS")
public class Weathers {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "wind_speed")
    private Double windSpeed;

    @Column(name = "wind_dir")
    private Double windDir;

    @Column(name = "sunset")
    private String sunset;

    @Column(name = "sunrise")
    private String sunrise;

    @Column(name = "temp")
    private Double temp;

    @Column(name = "date_time")
    private LocalDateTime dateTime;

    @Column(name = "sysdate")
    private LocalDateTime sysdate;
}
