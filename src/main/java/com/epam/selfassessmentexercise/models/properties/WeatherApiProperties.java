package com.epam.selfassessmentexercise.models.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter

@Configuration
@ConfigurationProperties("weather.api")
public class WeatherApiProperties {

    private String key;
    private String domain;
    private String version;

    private String endPointCurrentWeather;
}
