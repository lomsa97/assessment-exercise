package com.epam.selfassessmentexercise.models.responses.weather.api;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class CurrentWeatherResponse {

    private List<CurrentWeatherModelResponse> data;
    private Integer count;
}
