package com.epam.selfassessmentexercise.models.responses.weather.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class CurrentWeatherModelResponse {

    @JsonProperty("timezone")
    private String timezone;
    @JsonProperty("country_code")
    private String countryCode;
    @JsonProperty("state_code")
    private String stateCode;
    @JsonProperty("city_name")
    private String cityName;
    @JsonProperty("wind_spd")
    private Double windSpeed;
    @JsonProperty("wind_dir")
    private Double windDir;
    @JsonProperty("sunset")
    private String sunset;
    @JsonProperty("sunrise")
    private String sunrise;
    @JsonProperty("temp")
    private Double temp;

    @JsonProperty("ob_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateTime;
}
