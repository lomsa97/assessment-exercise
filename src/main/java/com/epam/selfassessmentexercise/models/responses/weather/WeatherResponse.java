package com.epam.selfassessmentexercise.models.responses.weather;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class WeatherResponse {

    @JsonProperty("country_name")
    private String countryName;

    @JsonProperty("city_name")
    private String cityName;

    @JsonProperty("wind_spd")
    private Double windSpeed;

    @JsonProperty("wind_dir")
    private Double windDir;

    @JsonProperty("sunset")
    private String sunset;

    @JsonProperty("sunrise")
    private String sunrise;

    @JsonProperty("temp")
    private Double temp;

    @JsonProperty("time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime dateTime;

    @JsonProperty("sysdate")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime sysdate;
}
