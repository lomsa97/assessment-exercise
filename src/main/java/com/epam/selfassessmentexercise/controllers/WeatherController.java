package com.epam.selfassessmentexercise.controllers;

import com.epam.selfassessmentexercise.models.requests.CurrentWeatherRequest;
import com.epam.selfassessmentexercise.models.responses.weather.WeatherResponse;
import com.epam.selfassessmentexercise.services.weather.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("weather")
public class WeatherController {

    public WeatherService weatherService;

    @Autowired
    public WeatherController(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    @GetMapping("current")
    public WeatherResponse fetchDataFromCurrentWeather(@Valid CurrentWeatherRequest currentWeatherRequest) {
        return weatherService.fetchDataFromCurrentWeather(currentWeatherRequest);
    }

    @GetMapping("history")
    public List<WeatherResponse> getCurrentWeather() {
        return weatherService.getSavedHistoryWeatherData();
    }
}
