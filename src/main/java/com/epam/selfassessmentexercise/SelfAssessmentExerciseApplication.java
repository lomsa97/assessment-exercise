package com.epam.selfassessmentexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelfAssessmentExerciseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SelfAssessmentExerciseApplication.class, args);
    }

}
