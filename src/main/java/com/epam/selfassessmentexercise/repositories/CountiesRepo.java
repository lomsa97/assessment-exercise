package com.epam.selfassessmentexercise.repositories;

import com.epam.selfassessmentexercise.models.entities.Countries;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CountiesRepo extends CrudRepository<Countries, Long> {

    @EntityGraph(attributePaths = {"cities", "cities.weathers"})
    List<Countries> findAll();

    @EntityGraph(attributePaths = {"cities", "cities.weathers"})
    Countries findByName(String name);
}
